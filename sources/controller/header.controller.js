app.controller('HeaderController', ['$scope',
  function($scope) {
    var vm = this;

    $scope.navCollapsed = true;
    $scope.toggleCollapsed = function() {
        $scope.navCollapsed = true;
    };
  }
]);

app.directive('scrollNav', function ($window) {
  return function(scope, element, attrs) {
    angular.element($window).bind("scroll", function() {
      if (this.pageYOffset >= 300) {
        scope.scrollDown = true;
      } else {
        scope.scrollDown = false;
      }
      scope.$apply();
    });
  };
});