app.controller('HomeController', ['$scope', function($scope) {
    var vm = this;
  }
]);

app.controller('VideoController', ['$uibModal', function($uibModal) {
    var ctrl = this;

    ctrl.open1 = function (size) {
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'modal1',
        size: size
      });
    };

    ctrl.open2 = function (size) {
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'modal2',
        size: size
      });
    };
  }
]);

app.directive('tooltip', function(){
    return {
        restrict: 'A',
        link: function(scope, element, attrs){
            $(element).hover(function(){
                // on mouseenter
                $(element).tooltip('show');
            }, function(){
                // on mouseleave
                $(element).tooltip('hide');
            });
        }
    };
});

app.controller('formController', function ($scope, $filter, Notification, $timeout) {
    $scope.searchButtonText = "SEND";

    $scope.submitForm = function(user) {
        $scope.searchButtonText = "SENDING";

        if ($scope.userForm.$valid) {
            var service_id = 'gmail';
            var template_id = 'xcidic_emailtemplate';
            var template_params = {
                from_name: user.name,
                from_email: user.email,
                event_date: $filter('date')(user.event_date, "dd-MM-yyyy"),
                hear_from: user.hear_from,
                contact_number: user.contact_no,
                message: user.message
            };

            emailjs.send(service_id,template_id,template_params)
            .then(function(response) {
              Notification.success({message: 'Your message was sent successfully. Thanks.', positionY: 'bottom', positionX: 'right'});
                $timeout(function(){
                   window.location.reload();
                }, 5000);
            }, function(err) {
              Notification.error({message: 'Failed to send your message. Please try again.', positionY: 'bottom', positionX: 'right'});
              $scope.searchButtonText = "SEND";
            });
        }
    };

  $scope.dateOptions = {
    showButtonBar: false,
    formatYear: 'yy',
    minDate: new Date(),
    startingDay: 1
  };

  $scope.open = function() {
    $scope.popup.opened = true;
  };

  $scope.formats = ['dd-MM-yyyy', 'dd MMMM yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  $scope.format = $scope.formats[0];

  $scope.popup = {
    opened: false
  };
});