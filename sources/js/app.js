var app = angular.module('stanleyApp', ['ngAnimate', 'ui.router', 'ui.bootstrap', 'duScroll', 'ui-notification']);

app.config(function($stateProvider, $urlRouterProvider, $locationProvider) {
  $urlRouterProvider.otherwise('/');  

  $stateProvider
  .state('layout', {
    abstract: true,
    views: {
      'header': {
        templateUrl: 'sources/views/header.html',
        controller: 'HeaderController',
        controllerAs: 'vm'
      },
      'content': {
        template:'<ui-view/>'
      },
      'footer': {
        templateUrl: 'sources/views/footer.html'
      }
    }
  })
  .state('layout.home', {
    url: '/',
    templateUrl: 'sources/views/home.html',
    controller: 'HomeController',
    controllerAs: 'vm'
  });

  $locationProvider.html5Mode(true);
});